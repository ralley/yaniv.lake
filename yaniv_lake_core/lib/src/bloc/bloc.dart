import 'package:meta/meta.dart';
import 'package:yaniv_lake_core/src/model/photo.dart';
import 'package:yaniv_lake_core/yaniv_lake_core.dart';

abstract class Disposable {
  void dispose();
}

abstract class Bloc extends Disposable {}

class MainBloc extends Bloc {
  LocationRepository locationRepository;
  UserRepository userRepository;
  GallaryRepository gallaryRepository;

  MainBloc(
      {@required LocationRepository this.locationRepository,
      @required UserRepository this.userRepository,
      @required GallaryRepository this.gallaryRepository});

  @override
  void dispose() {
    // TODO: implement dispose
  }
}

class LoginBloc extends Bloc {
  UserRepository userRepository;
  
  bool get isLoggedIn { return true; }

  LoginBloc({@required UserRepository this.userRepository});

  @override
  void dispose() {
    // TODO: implement dispose
  }
}

class GallaryBloc extends Bloc {
  GallaryRepository gallaryRepository;

  GallaryBloc({@required GallaryRepository this.gallaryRepository});

  @override
  void dispose() {
    // TODO: implement dispose
  }

  Future<Picture> getPhoto(int index) async {
    final id = "photo_${index % 13 + 1}.jpg";
    return gallaryRepository.getPhoto(id);
  }
}