import 'dart:async';
import 'package:yaniv_lake_core/src/bloc/bloc.dart';
import 'package:yaniv_lake_core/src/model/user.dart';

abstract class UserRepository extends Disposable {
  Future<User> login();
}