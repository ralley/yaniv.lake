import 'dart:async';
import 'package:yaniv_lake_core/src/bloc/bloc.dart';
import 'package:yaniv_lake_core/src/model/photo.dart';

abstract class GallaryRepository extends Disposable {
  Future<Picture> getPhoto(String id);
}