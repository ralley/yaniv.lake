import 'dart:async';
import 'package:yaniv_lake_core/src/bloc/bloc.dart';
import 'package:yaniv_lake_core/src/model/contacts.dart';
import 'package:yaniv_lake_core/src/model/location.dart';

abstract class LocationRepository extends Disposable {
  Stream<List<Location>> locations();
  Future<Contacts> contacts();
}