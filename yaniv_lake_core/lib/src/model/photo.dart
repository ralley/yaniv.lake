import 'dart:io';

class Photo {
  String url;
  String name;

  Photo({
    this.url,
    this.name,
  });

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        url: json["url"] == null ? null : json["url"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "url": url == null ? null : url,
        "name": name == null ? null : name,
      };
}

class Picture {
  String id;
  String name;
  File file;
  File thumbnail;

  Picture({
    this.id,
    this.name,
    this.file,
    this.thumbnail,
  });
}
