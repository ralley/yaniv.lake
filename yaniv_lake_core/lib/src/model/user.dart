// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

User userFromJson(String str) {
  final jsonData = json.decode(str);
  return User.fromJson(jsonData);
}

String userToJson(User data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class User {
  String id;
  String displayName;
  String photoUrl;

  User({
    this.id,
    this.displayName,
    this.photoUrl,
  });

  factory User.fromJson(Map<String, dynamic> json) => new User(
        id: json["id"] == null ? null : json["id"],
        displayName: json["displayName"] == null ? null : json["displayName"],
        photoUrl: json["photoUrl"] == null ? null : json["photoUrl"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "displayName": displayName == null ? null : displayName,
        "photoUrl": photoUrl == null ? null : photoUrl,
      };
}
