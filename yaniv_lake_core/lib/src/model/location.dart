import 'dart:convert';

Location locationFromJson(String str) {
  final jsonData = json.decode(str);
  return Location.fromJson(jsonData);
}

String locationToJson(Location data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class Location {
  String id;
  double latitude;
  double longitude;
  Prices prices;
  List<String> photos;
  String cover;

  Location({
    this.id,
    this.latitude,
    this.longitude,
    this.prices,
    this.photos,
    this.cover,
  });

  factory Location.fromJson(Map<String, dynamic> json) => new Location(
        id: json["id"] == null ? null : json["id"],
        latitude: json["latitude"] == null ? null : json["latitude"].toDouble(),
        longitude:
            json["longitude"] == null ? null : json["longitude"].toDouble(),
        prices: json["prices"] == null ? null : Prices.fromJson(json["prices"]),
        photos: json["photos"] == null
            ? null
            : new List<String>.from(json["photos"].map((x) => x)),
        cover: json["cover"] == null ? null : json["cover"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "prices": prices == null ? null : prices.toJson(),
        "photos": photos == null
            ? null
            : new List<dynamic>.from(photos.map((x) => x)),
        "cover": cover == null ? null : cover,
      };
}

class Prices {
  int restPlace;
  int fishingPlace;

  Prices({
    this.restPlace,
    this.fishingPlace,
  });

  factory Prices.fromJson(Map<String, dynamic> json) => new Prices(
        restPlace: json["restPlace"] == null ? null : json["restPlace"],
        fishingPlace:
            json["fishingPlace"] == null ? null : json["fishingPlace"],
      );

  Map<String, dynamic> toJson() => {
        "restPlace": restPlace == null ? null : restPlace,
        "fishingPlace": fishingPlace == null ? null : fishingPlace,
      };
}
