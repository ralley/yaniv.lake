import 'dart:convert';

Contacts contactsFromJson(String str) {
  final jsonData = json.decode(str);
  return Contacts.fromJson(jsonData);
}

String contactsToJson(Contacts data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class Contacts {
  String viber;
  String facebook;
  String mail;
  String phone;

  Contacts({
    this.viber,
    this.facebook,
    this.mail,
    this.phone,
  });

  factory Contacts.fromJson(Map<String, dynamic> json) => new Contacts(
        viber: json["viber"] == null ? null : json["viber"],
        facebook: json["facebook"] == null ? null : json["facebook"],
        mail: json["mail"] == null ? null : json["mail"],
        phone: json["phone"] == null ? null : json["phone"],
      );

  Map<String, dynamic> toJson() => {
        "viber": viber == null ? null : viber,
        "facebook": facebook == null ? null : facebook,
        "mail": mail == null ? null : mail,
        "phone": phone == null ? null : phone,
      };
}
