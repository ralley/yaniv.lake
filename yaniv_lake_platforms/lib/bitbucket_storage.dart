import 'dart:async';
import 'dart:core';
import 'dart:io';
import 'dart:typed_data' show Uint8List;
import 'package:flutter/foundation.dart'; 
import 'package:yaniv_lake_core/yaniv_lake_core.dart' show GallaryRepository, Picture;
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class BitbucketRepository implements GallaryRepository {
  static const String _storageUrl =
      'https://bitbucket.org/ralley/yaniv.lake/raw/9b22ab427f49f6867da1870636c8173c6f9be816/photos/';

  @override
  void dispose() {}

  @override
  Future<Picture> getPhoto(String id) async {
    Future<File> _fetchFile(String url, bool thumbnail) async {
      final manager = DefaultCacheManager();
      final key = "$url?thumbnail=$thumbnail.compressed";
      var cached = await manager.getFileFromCache(key);
      if (cached == null) {
        final fileBytes = await compute(_fetchImage, url);
        if (thumbnail) {
          final thumbnailBytes = await FlutterImageCompress.compressWithList(
            fileBytes,
            minWidth: 200,
            minHeight: 200,
            quality: 95,
          );
          return await manager.putFile(key, Uint8List.fromList(thumbnailBytes));
        } else {
          final fullHdBytes = await FlutterImageCompress.compressWithList(
            fileBytes,
            minWidth: 1280,
            minHeight: 720,
            quality: 85,
          );
           return await manager.putFile(key, Uint8List.fromList(fullHdBytes));
        }
      }
      return cached.file;
    }

    final url = "$_storageUrl$id";
    final originalFile = await _fetchFile(url, false);
    final thumbnailFile = await _fetchFile(url, true);

    return Picture(
      id: url,
      name: id,
      file: originalFile,
      thumbnail: thumbnailFile,
    );
  }

  static Future<Uint8List> _fetchImage(String url) async {
    final httpClient = http.Client();
    final response = await httpClient.get(url);
    httpClient.close();
    return response.bodyBytes;
  }
}
