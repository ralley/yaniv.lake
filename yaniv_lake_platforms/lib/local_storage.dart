import 'package:yaniv_lake_core/yaniv_lake_core.dart';

class InMemoryLocationRepository extends LocationRepository {
  @override
  void dispose() {}

  @override
  Future<Contacts> contacts() async {
    var constacts = Contacts(
        facebook: "facebook",
        mail: "mail@mail.com",
        phone: "911",
        viber: "viber");
    return await Future.value(constacts);
  }

  @override
  Stream<List<Location>> locations() async* {
    yield <Location>[
      Location(),
    ];
  }
}
class InMemoryUserRepository extends UserRepository {
  @override
  void dispose() {}

  @override
  bool get isLoggedIn => true;

  @override
  Future<User> login() async {
    var user = User(
      id: "",
      displayName: "Dmytro Kravchyna",
      photoUrl: "",
    );
    return await Future.value(user);
  }
}
class InMemoryGallaryRepository extends GallaryRepository {
  @override
  void dispose() { }

  @override
  Future<Picture> getPhoto(String id) {
    // TODO: implement getPhoto
    return null;
  }
}