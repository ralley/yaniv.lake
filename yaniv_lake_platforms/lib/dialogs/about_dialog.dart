import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:yaniv_lake_mobile/widgets/link_text_span.dart';

void showAboutAppDialog(BuildContext context) {
  final ThemeData themeData = Theme.of(context);
  final TextStyle aboutTextStyle = themeData.textTheme.body2;
  final TextStyle linkStyle =
      themeData.textTheme.body2.copyWith(color: themeData.accentColor);

  showAboutDialog(
    context: context,
    applicationLegalese: '© 2019 Dmytro Kravchyna',
    applicationName: "Fisher's club",
    applicationVersion: 'April 2019',
    applicationIcon: Container(
      height: 82.0,
      width: 82.0,
      child: FlareActor(
        "animations/lake.flr",
        alignment: Alignment.center,
        fit: BoxFit.contain,
        animation: "intro",
        isPaused: true,
      ),
    ),
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.only(top: 24.0),
        child: RichText(
          text: TextSpan(
            children: <TextSpan>[
              TextSpan(
                style: aboutTextStyle,
                text: 'Flutter is an open-source project to help developers '
                    'build high-performance, high-fidelity, mobile apps for '
                    'from a single codebase. This design lab is a playground '
                    "and showcase of Flutter's many widgets, behaviors, "
                    'animations, layouts, and more. Learn more about Flutter at ',
              ),
              LinkTextSpan(
                style: linkStyle,
                url: 'https://flutter.dev',
              ),
              TextSpan(
                style: aboutTextStyle,
                text:
                    '.\n\nTo see the source code for this app, please visit the ',
              ),
              TextSpan(
                style: linkStyle,
                text: 'flutter github repo',
              ),
              TextSpan(
                style: aboutTextStyle,
                text: '.',
              ),
            ],
          ),
        ),
      ),
    ],
  );
}

void showMembershipInfoDialog(BuildContext context) {
  final ThemeData themeData = Theme.of(context);
  final TextStyle aboutTextStyle = themeData.textTheme.body2;
  final TextStyle linkStyle =
      themeData.textTheme.body2.copyWith(color: themeData.accentColor);

  showAboutDialog(
    context: context,
    applicationIcon: Container(
      height: 82.0,
      width: 82.0,
      child: FlareActor(
          "animations/athlete.flr",
          alignment: Alignment.bottomRight,
          callback: (name) {},
          fit: BoxFit.contain,
          animation: "loading",
        ),
    ),
    applicationVersion: 'Flutter is an open-source project to help developers '
                    'build high-performance, high-fidelity, mobile apps for '
                    'animations, layouts, and more. Learn more about Flutter at ',
    applicationName: "Fisher's membership",
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.only(top: 24.0),
        child: RichText(
          text: TextSpan(
            children: <TextSpan>[
              TextSpan(
                style: aboutTextStyle,
                text: 'Flutter is an open-source project to help developers '
                    'build high-performance, high-fidelity, mobile apps for '
                    'from a single codebase. This design lab is a playground '
                    "and showcase of Flutter's many widgets, behaviors, "
                    'animations, layouts, and more. Learn more about Flutter at ',
              ),
              LinkTextSpan(
                style: linkStyle,
                url: 'https://flutter.dev',
              ),
              TextSpan(
                style: aboutTextStyle,
                text:
                    '.\n\nTo see the source code for this app, please visit the ',
              ),
              TextSpan(
                style: linkStyle,
                text: 'flutter github repo',
              ),
              TextSpan(
                style: aboutTextStyle,
                text: '.',
              ),
            ],
          ),
        ),
      ),
    ],
  );
}
