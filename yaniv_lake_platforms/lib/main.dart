import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:yaniv_lake_core/yaniv_lake_core.dart';
import 'package:yaniv_lake_mobile/dependency_injector.dart';
import 'package:yaniv_lake_mobile/screens/gallary_screen.dart';
import 'package:yaniv_lake_mobile/screens/login_screen.dart';
import 'package:yaniv_lake_mobile/screens/main_screen.dart';
import 'package:yaniv_lake_mobile/keys.dart';
import 'bitbucket_storage.dart';
import 'local_storage.dart';

void main() => root(
      userRepository: InMemoryUserRepository(),
      gallaryRepository: BitbucketRepository(),
      locationRepository: InMemoryLocationRepository(),
    );

void root({
  @required LocationRepository locationRepository,
  @required UserRepository userRepository,
  @required GallaryRepository gallaryRepository,
}) {
  bool isLoggedIn = true;
  runApp(
    Injector(
      gallaryRepository: gallaryRepository,
      locationRepository: locationRepository,
      userRepository: userRepository,
      child: MaterialApp(
        title: "Fisher's Lake", //BlocLocalizations().appTitle,
        theme: ThemeData.light(), //ArchSampleTheme.theme,
        initialRoute: isLoggedIn ? Routes.main : Routes.login,
        routes: {
          Routes.main: (context) {
            return MainScreen(
              bloc: Injector.of(context).mainBloc,
            );
          },
          Routes.login: (context) {
            return LoginScreen(
              bloc: Injector.of(context).loginBloc,
            );
          },
          Routes.gallary: (context) {
            return GallaryScreen(
              bloc: Injector.of(context).gallaryBloc,
            );
          }
        },
      ),
    ),
  );
}
