library dependency_injector;

import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';
import 'package:yaniv_lake_core/yaniv_lake_core.dart' 
show GallaryRepository, LocationRepository, UserRepository,
  GallaryBloc, LoginBloc, MainBloc;

class Injector extends InheritedWidget {
  final LocationRepository locationRepository;
  final UserRepository userRepository;
  final GallaryRepository gallaryRepository;

  final MainBloc mainBloc;
  final LoginBloc loginBloc;
  final GallaryBloc gallaryBloc;

  Injector({
    Key key,
    @required this.locationRepository,
    @required this.userRepository,
    @required this.gallaryRepository,
    @required Widget child,
  }) : mainBloc = MainBloc(gallaryRepository: gallaryRepository, locationRepository: locationRepository, userRepository: userRepository),
       loginBloc = LoginBloc(userRepository: userRepository),
       gallaryBloc = GallaryBloc(gallaryRepository: gallaryRepository), 
       super(key: key, child: child);

  static Injector of(BuildContext context) =>
      context.inheritFromWidgetOfExactType(Injector);

  @override
  bool updateShouldNotify(Injector oldWidget) =>
      locationRepository != oldWidget.locationRepository ||
      userRepository != oldWidget.userRepository ||
      gallaryRepository != oldWidget.gallaryRepository;
}
