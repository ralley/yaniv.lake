
import 'package:flutter/widgets.dart';

class EmptyText extends Text{
  const EmptyText(): super("",style: const TextStyle(height: 0.0,));
}