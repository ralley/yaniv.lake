import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:yaniv_lake_core/yaniv_lake_core.dart';
import 'package:yaniv_lake_mobile/keys.dart';
import 'package:yaniv_lake_mobile/screens/main_screen.dart';
import 'package:yaniv_lake_mobile/widgets/fisher_icons_icons.dart';

class LoginScreen extends StatelessWidget {
  final LoginBloc bloc;

  LoginScreen({Key key, @required this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //final bloc = LoginBloc();
    return Scaffold(
      body: Material(
        color: Theme.of(context).primaryColor,
        child: Stack(
          fit: StackFit.passthrough,
          alignment: Alignment.topLeft,
          children: <Widget>[
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(32),
                child: Text(
                  "Fisher's lake",
                  style: TextStyle(
                      color: Theme.of(context).primaryTextTheme.subhead.color,
                      fontSize: Theme.of(context).textTheme.display3.fontSize,
                      fontWeight: FontWeight.w800),
                  maxLines: 2,
                ),
              ),
            ),
            Center(
              child: SizedBox.fromSize(
                size: Size.fromWidth(
                    MediaQuery.of(context).size.shortestSide * 0.75),
                child: FlareActor(
                  "animations/lake.flr",
                  alignment: Alignment.center,
                  fit: BoxFit.scaleDown,
                  animation: "intro",
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: SafeArea(
        bottom: true,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 32),
          child: FloatingActionButton.extended(
            heroTag: "login_bttn",
            backgroundColor: const Color.fromARGB(255, 59, 89, 152),
            label: const Text("Login with facebook"),
            icon: const Icon(FisherIcons.facebook),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(Routes.login);
            },
          ),
        ),
      ),
    );
  }
}
