import 'package:flutter/material.dart';
import 'package:yaniv_lake_mobile/keys.dart';
import 'package:yaniv_lake_core/yaniv_lake_core.dart';
import 'package:url_launcher/url_launcher.dart' as urlLauncher;

class LocationPage extends StatefulWidget {
  final String title;
  final MainBloc bloc;

  LocationPage({Key key, this.title, @required this.bloc}) : super(key: key);

  _LocationPageState createState() => _LocationPageState();
}

class _LocationPageState extends State<LocationPage> {
  @override
  Widget build(BuildContext context) {
    return widget.title == null
        ? _dragHeader(context)
        : Scaffold(
            appBar: AppBar(title: Text(widget.title)),
            body: _body(context),
          );
  }

  Widget _dragHeader(BuildContext context) {
    return Material(
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          boxShadow: const <BoxShadow>[
            BoxShadow(
              blurRadius: 6.0,
              offset: Offset(-3, -3),
              //spreadRadius: 6.0,
              color: Color.fromRGBO(0, 0, 0, 0.25),
            ),
          ],
          borderRadius: const BorderRadius.vertical(
              top: Radius.circular(12.0), bottom: Radius.zero),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            ListTile(
              dense: true,
              leading: Icon(
                Icons.drag_handle,
                color: Theme.of(context).buttonColor,
              ),
              title: Text(
                "Place 1",
                maxLines: 1,
                style: Theme.of(context).primaryTextTheme.title,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  color: Colors.white, //Colors.grey[300],
                  borderRadius: BorderRadius.all(Radius.circular(12))),
              child: Padding(
                padding: const EdgeInsets.only(top: 12.0),
                child: _body(context),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _body(BuildContext context) {
    return Hero(
      tag: "detailInfoHero",
      child: SingleChildScrollView(
        child: Material(
          child: Column(
            children: <Widget>[
              ListTile(
                dense: true,
                leading: Icon(Icons.nature_people),
                title: Text(
                  "Pavilion",
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.body1,
                ),
                trailing: Text("20"),
              ),
              Divider(),
              ListTile(
                dense: true,
                leading: Icon(Icons.rowing),
                title: Text(
                  "Fishing Dam",
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.body1,
                ),
                trailing: Text("+ 20"),
              ),
              Divider(),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  ListTile(
                    dense: true,
                    title: Text(
                      "Images",
                      style: Theme.of(context).textTheme.subtitle,
                    ),
                    trailing: IconButton(
                      alignment: Alignment.centerRight,
                      color: Theme.of(context).accentColor,
                      icon: Icon(Icons.more_horiz),
                      onPressed: () =>
                          Navigator.of(context).pushNamed(Routes.gallary),
                    ),
                  ),
                  Container(
                    height: 120,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: 3,
                      itemBuilder: (BuildContext context, int index) {
                        //return Container(height: 120, width: (width) / 2 - 2, color: Colors.red,margin: EdgeInsets.all(2),);
                        return Padding(
                          key: Key("detailImage$index"),
                          padding: EdgeInsets.all(2),
                          child: Image.network(
                            "https://cdn.pixabay.com/photo/2016/08/11/23/48/pnc-park-1587285_1280.jpg",
                            width: 200.0,
                            height: 120.0,
                            fit: BoxFit.cover,
                          ),
                        );
                      },
                    ),
                  ),
                  ListTile(
                    dense: true,
                    title: Text(
                      "Contact us",
                      style: Theme.of(context).textTheme.subtitle,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      //_button("Mail", Icons.mail),
                      _button("Viber", Icons.video_library, Colors.transparent,
                          () async {
                        const viberLink =
                            "viber://pa?chatURI=IwAR2US-rFWBFNT0_2lsdcFUf5xKSMFZgZrRMSy91DbpNVVrJBA6-430Pm3KA";
                        if (await urlLauncher.canLaunch(viberLink)) {
                          urlLauncher.launch(viberLink);
                        } else {
                          showDialog(
                              context: context,
                              builder: (c) {
                                return AlertDialog(
                                  content: new Text("Viber application is not installed"),
                                  actions: <Widget>[
                                    // usually buttons at the bottom of the dialog
                                    new FlatButton(
                                      child: new Text("Close"),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ],
                                );
                              });
                        }
                      }),
                      _button(
                          "Facebook", Icons.screen_rotation, Colors.transparent,
                          () {
                        urlLauncher.launch("https://www.facebook.com");
                      }),
                      _button("Phone", Icons.phone_iphone, Colors.transparent,
                          () {
                        urlLauncher.launch("tel:911");
                      }),
                    ],
                  ),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _button(String label, IconData icon,
      [Color color = Colors.transparent, VoidCallback action]) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        RawMaterialButton(
          shape: new CircleBorder(),
          fillColor: Theme.of(context).accentColor,
          constraints: BoxConstraints.tightFor(height: 56, width: 56),
          elevation: 6.0,
          highlightElevation: 12.0,
          key: Key(label),
          child: Icon(
            icon,
            color: Colors.white,
          ),
          onPressed: action ?? () {},
        ),
        SizedBox(
          height: 12.0,
        ),
        Text(
          label,
          style: Theme.of(context).textTheme.body1,
        ),
      ],
    );
  }
}
