import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import "package:flare_flutter/flare_actor.dart";
import 'package:yaniv_lake_core/yaniv_lake_core.dart';
export 'package:flare_dart/animation/actor_animation.dart';
export 'package:flare_dart/actor_node.dart';
import 'package:yaniv_lake_mobile/screens/controllers/assistant_controller.dart';
import 'package:yaniv_lake_mobile/dialogs/about_dialog.dart';
import 'package:yaniv_lake_mobile/widgets/flip_card.dart';

class DashbordPage extends StatefulWidget {
  final MainBloc bloc;
  DashbordPage({Key key, @required this.bloc}) : super(key: key);

  @override
  _DashbordPageState createState() => _DashbordPageState();
}

class _DashbordPageState extends State<DashbordPage> {
  AssistantController _assistantController;
  FlipCardController _flipController;

  @override
  void initState() {
    super.initState();
    _assistantController = AssistantController((settings) {
      settings.uri =
          'https://invite.viber.com/?g2=AQAmjja3%2BOCgGUlixu4OboFS5Q4ZhSu1OaZTvI50SRxbLp7sMCR1899umRyELmqP&fbclid=IwAR2US-rFWBFNT0_2lsdcFUf5xKSMFZgZrRMSy91DbpNVVrJBA6-430Pm3KA&lang=en';
    });
    _flipController = FlipCardController();
  }

  Widget _sliverAppBar(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      floating: true,
      snap: true,
      expandedHeight: 256,
      title: Text("Dashboard"),
      actions: <Widget>[
        FloatingActionButton(
          heroTag: "help_button",
          onPressed: () => showMembershipInfoDialog(context),
          elevation: 0,
          disabledElevation: 0,
          highlightElevation: 0,
          backgroundColor: Colors.transparent,
          child: Icon(
            Icons.help_outline,
            //color: Theme.of(context).textTheme.caption.color,
          ),
        )
      ],
      centerTitle: false,
      flexibleSpace: FlexibleSpaceBar(
        collapseMode: CollapseMode.parallax,
        centerTitle: false,
        background: Container(
          color: Theme.of(context).primaryColor,
          child: Column(
            children: <Widget>[
              AppBar(elevation: 0),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: FloatingActionButton(
                        heroTag: "flip_button",
                        onPressed: () {
                          _flipController.toogleCard();
                        },
                        elevation: 0,
                        highlightElevation: 0,
                        disabledElevation: 0,
                        backgroundColor: Colors.transparent,
                        child: Icon(
                          Icons.flip,
                          //color: Theme.of(context).textTheme.caption.color,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.only(bottom: 16),
                        transform: Matrix4.translationValues(9, 0, 0),
                        child: _flipCard(context),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _sliverRemainig(BuildContext context) {
    return SliverFillRemaining(
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(16, 32, 16, 16),
              child: Text.rich(
                TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                      text: "Sounds like you are new here. ",
                      style: Theme.of(context).textTheme.caption,
                    ),
                    TextSpan(
                        text: "Welcome to join us!",
                        style: Theme.of(context).textTheme.body1),
                  ],
                ),
              ),
            ),
            Expanded(
              child: GestureDetector(
                child: FlareActor(
                  "animations/user.flr",
                  shouldClip: true,
                  snapToEnd: true,
                  controller: _assistantController,
                  alignment: Alignment.bottomCenter,
                  fit: BoxFit.contain,
                ),
                onTap: () {
                  _assistantController.handle();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: OrientationBuilder(
        builder: (BuildContext c, Orientation orientation) {
          return CustomScrollView(
            //physics: ClampingScrollPhysics(),
            slivers: orientation == Orientation.portrait
                ? <Widget>[
                    _sliverAppBar(context),
                    _sliverRemainig(context),
                    // SliverList(
                    //   delegate: SliverChildListDelegate(List<Text>.generate(100, (int i) {
                    //     return Text("List item $i");
                    //   })),
                    // ),
                  ]
                : <Widget>[
                    _sliverAppBar(context),
                  ],
          );
        },
      ),
    );
  }

  FlipCard _flipCard(BuildContext context) {
    return FlipCard(
      controller: _flipController,
      direction: FlipDirection.horizontal,
      front: Card(
        elevation: 12,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Container(
          padding: const EdgeInsets.all(16),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              CircleAvatar(
                radius: 42,
                foregroundColor: Theme.of(context).primaryColor,
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(left: 16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Firstname Secondname Secondname Secondname Secondname",
                        style: Theme.of(context).textTheme.headline,
                        overflow: TextOverflow.fade,
                        maxLines: 3,
                      ),
                      Text(
                        "some_mail@mail.com",
                        style: Theme.of(context).textTheme.body1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      back: Card(
        color: Colors.white,
        elevation: 12,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Align(
          alignment: Alignment.center,
          child: QrImage(
            data: "Hello world!",
            padding: const EdgeInsets.all(32),
          ),
        ),
      ),
    );
  }
}
