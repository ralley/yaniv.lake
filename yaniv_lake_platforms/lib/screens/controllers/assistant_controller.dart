import 'package:flare_flutter/flare_controls.dart';
import 'package:flare_flutter/flare.dart';
export 'package:flare_dart/animation/actor_animation.dart';
export 'package:flare_dart/actor_node.dart';
import 'package:url_launcher/url_launcher.dart' as urlLauncher;

class AssistantSettings {
  String uri;

  AssistantSettings([this.uri]);
}

class AssistantController extends FlareControls {
  static const String idle_animation = "idle";
  static const String move_phone_animation = "move_phone";
  static const String phone_sway_animation = "phone_sway";

  final AssistantSettings _settings;
  bool _isSwaying = false;

  AssistantController(Function(AssistantSettings) buider)
      : _settings = AssistantSettings(),
        super() {
    buider(_settings);
  }

  @override
  void initialize(FlutterActorArtboard artboard) {
    super.initialize(artboard);
    //_phoneNode = artboard.getNode("screen"); //phone
    _idle();
  }

  @override
  void onCompleted(String name) {
    super.onCompleted(name);
    if (name == move_phone_animation) {
      _phoneSway();
    } else if (name == idle_animation) {
      _idle();
    } else {
      _phoneSway();
    }
  }

  void _idle() {
    _isSwaying = false;
    play(idle_animation);
  }

  void _phoneSway() {
    _isSwaying = true;
    play(phone_sway_animation);
  }

  void _movePhone() {
    _isSwaying = false;
    play(move_phone_animation);
  }

  void handle() async {
    if (!_isSwaying) {
      _movePhone();
    } else {
      final viberUrl = _settings?.uri;
      if (viberUrl != null && await urlLauncher.canLaunch(viberUrl)) {
        await urlLauncher.launch(viberUrl);
      } else {
        throw 'Could not launch $viberUrl';
      }
    }
  }
}
