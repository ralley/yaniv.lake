import 'package:flutter/material.dart';
import 'package:yaniv_lake_core/yaniv_lake_core.dart' show GallaryBloc;
import 'package:yaniv_lake_core/src/model/photo.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';

class GallaryScreen extends StatelessWidget {
  final GallaryBloc bloc;
  GallaryScreen({Key key, @required this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Gallary"),
      ),
      body: Material(
        child: OrientationBuilder(
          builder: (BuildContext context, Orientation orientation) {
            return GridView.builder(
              itemCount: 13,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: orientation == Orientation.portrait ? 4 : 7),
              itemBuilder: (BuildContext context, int index) {
                return FutureBuilder(
                    future: bloc.getPhoto(index),
                    builder: (context, snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                        case ConnectionState.waiting:
                          return Center(
                            child: CircularProgressIndicator(),
                            heightFactor: 0.5,
                            widthFactor: 0.5,
                          );
                        default:
                          if (snapshot.hasError)
                            return Text('Error: ${snapshot.error}');
                          else
                            return Padding(
                              padding: EdgeInsets.all(2),
                              child: GestureDetector(
                                child: _gallaryPhotoTemplate(
                                    context, snapshot.data, index, true),
                                onTap: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (c) => GallaryPhotoScreen(
                                            photo: snapshot.data,
                                            index: index)),
                                  );
                                },
                              ), //onPressed: () { },
                            );
                      }
                    });
              },
            );
          },
        ),
      ),
    );
  }
}

class GallaryPhotoScreen extends StatelessWidget {
  final Picture photo;
  final int index;

  GallaryPhotoScreen({@required this.photo, this.index});

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        appBar: AppBar(
          title: Text("image ${photo.name}"),
        ),
        body: Center(
          child: Dismissible(
            key: Key("${photo.name}dismissable"),
            direction: DismissDirection.down,
            confirmDismiss: (direction) {
              Navigator.of(context).pop(false);
              return Future.value(false);
            },
            child: _gallaryPhotoTemplate(context, photo, index, false),
          ),
        ),
      ),
    );
  }
}

Widget _gallaryPhotoTemplate(BuildContext context, Picture photo,
    [int index, bool low = false]) {
  final tag = "${photo.name}$index";
  return Hero(
    tag: tag,
    key: Key(tag),
    child: Image(
      image: low ? FileImage(photo.thumbnail) : FileImage(photo.file),
      filterQuality: FilterQuality.low,
      fit: BoxFit.cover,
    ),
  );
}
 