import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:yaniv_lake_core/yaniv_lake_core.dart';
import 'package:yaniv_lake_mobile/keys.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:yaniv_lake_mobile/dialogs/about_dialog.dart';
import 'package:yaniv_lake_mobile/screens/location_page.dart';

class BrowsePage extends StatefulWidget {
  final MainBloc bloc;
  BrowsePage({Key key, @required this.bloc}) : super(key: key);

  _BrowsePageState createState() => _BrowsePageState();
}

class _BrowsePageState extends State<BrowsePage> {
  bool _navigating = false;
  final _controller = PanelController();

  static const shadowHeight = 24.0;
  static const paddings = 16.0;
  static const fabSize = 56;
  static const forceToNavigate = 5;
  static const panelHeightOpen = 587.0;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          OrientationBuilder(
            builder: (BuildContext context, Orientation orientation) {
              final level = orientation == Orientation.portrait ? 2 : 1;
              final panelHeightClosed =
                  52.0 - 1.0 + level * 60.0 + fabSize + shadowHeight;
              return SlidingUpPanel(
                maxHeight: panelHeightOpen,
                minHeight: panelHeightClosed,
                parallaxEnabled: true,
                controller: _controller,
                parallaxOffset: .5,
                color: Colors.transparent,
                boxShadow: null,
                body: Stack(
                  children: <Widget>[
                    _body(),
                    Positioned(
                      right: MediaQuery.of(context).padding.right,
                      top: MediaQuery.of(context).padding.top,
                      child: FloatingActionButton(
                        heroTag: "about_button",
                        onPressed: () => showAboutAppDialog(context),
                        child: Icon(
                          Icons.help_outline,
                        ),
                      ),
                    ),
                    Positioned(
                      right: MediaQuery.of(context).padding.right +
                          (orientation == Orientation.portrait ? 0.0 : 64.0),
                      top: MediaQuery.of(context).padding.top +
                          (orientation == Orientation.portrait ? 64.0 : 0.0),
                      child: FloatingActionButton(
                        heroTag: "photo_library_button",
                        onPressed: () => Navigator.of(context).pushNamed(Routes.gallary),
                        mini: true,
                        child: Icon(
                          Icons.photo_library,
                          size: 24,
                        ),
                      ),
                    ),
                  ],
                ),
                panel: Column(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.topLeft,
                      height: fabSize + shadowHeight,
                      child: ListView.builder(
                        padding: EdgeInsets.only(
                            left: paddings - 4, right: paddings - 4),
                        physics: ClampingScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: 12,
                        itemBuilder: (BuildContext context, int index) =>
                            Padding(
                              padding:
                                  EdgeInsets.fromLTRB(4, 0, 8, shadowHeight),
                              child: FloatingActionButton(
                                heroTag: "item$index",
                                key: Key("item$index"),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                child: Text("$index"),
                                onPressed: () {
                                  // Navigator.push(context, MaterialPageRoute(
                                  //     builder: (BuildContext context) {
                                  //   return DetailedPagePage();
                                  // }));
                                },
                              ),
                            ),
                      ),
                    ),
                    LocationPage(),
                  ],
                ),
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(18),
                    topRight: Radius.circular(18)),
                onPanelSlide: (double pos) async {
                  print("$pos");
                },
                onPanelOpened: () {
                  final screenHeight = MediaQuery.of(context).size.height;
                  if (!_navigating && panelHeightOpen >= screenHeight) {
                    _navigating = true;
                    _controller.close();

                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => LocationPage(title: "Place 1")),
                    );
                    _navigating = false;
                  }
                  print("onPanelOpened");
                },
                onPanelClosed: () {
                  print("onPanelClosed");
                },
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _body() {
    final CameraPosition _kGooglePlex = CameraPosition(
      target: LatLng(37.42796133580664, -122.085749655962),
      zoom: 14.4746,
    );
    return GoogleMap(
      mapType: MapType.normal,
      initialCameraPosition: _kGooglePlex,
      // onMapCreated: (GoogleMapController controller) {
      //   //_controller = controller;
      // },
      rotateGesturesEnabled: true,
      scrollGesturesEnabled: true,
      zoomGesturesEnabled: true,
      tiltGesturesEnabled: false,
      markers: <Marker>{
        Marker(
          position: _kGooglePlex.target,
          markerId: MarkerId("1"),
          flat: true,
          infoWindow: InfoWindow(title: "Label 1", snippet: "Label"),
          rotation: 23,
        )
      },
    );
  }
}
