import 'package:flutter/material.dart';
import 'package:yaniv_lake_core/yaniv_lake_core.dart';
import 'package:yaniv_lake_mobile/screens/browse_screen.dart';
import 'package:yaniv_lake_mobile/screens/dashbord_screen.dart';
import 'package:yaniv_lake_mobile/widgets/empty_text.dart';

class MainScreen extends StatefulWidget {
  final MainBloc bloc;

  MainScreen({Key key, @required this.bloc}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _index = 0;
  List<Widget> _pages;
  PageStorageBucket _bucket;

  @override
  void initState() {
    super.initState();
    _bucket = PageStorageBucket();
    _pages = [
      DashbordPage(key: PageStorageKey('/main/dashbord'), bloc: widget.bloc),
      BrowsePage(key: PageStorageKey('/main/browse'), bloc: widget.bloc),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        child: _pages[_index],
        bucket: _bucket,
      ),
      bottomNavigationBar: _bottomNavigationBar(),
    );
  }

  BottomNavigationBar _bottomNavigationBar() {
    return BottomNavigationBar(
      currentIndex: _index,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(icon: Icon(Icons.home), title: EmptyText()),
        BottomNavigationBarItem(icon: Icon(Icons.info), title: EmptyText())
      ],
      onTap: (i) {
        if (_index != i) setState(() => _index = i);
      },
    );
  }
}
